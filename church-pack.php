<?php
/*
Plugin Name: Church Pack
Plugin URI: http://wpforchurch.com
Description: Add groups, people (staff or all members), events, and more to your WordPress site. Visit <a href="http://wpforchurch.com" target="_blank">Wordpress for Church</a> for tutorials and support.
Version: 1.1
Author: Jack Lamb
Author URI: http://wpforchurch.com/
License: GPL
*/

// Translations
function wpfc_churchpack_translations() {
	load_plugin_textdomain( 'churchpack', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'wpfc_churchpack_translations' );

// Include Options
require_once plugin_dir_path( __FILE__ ) . '/options.php';

// Call options array
$coreoptions = get_option('cpo_core_options');
$options = get_option('cpo_options');

// Include Groups
if ( $coreoptions['groups'] == '1' ) { 
require_once plugin_dir_path( __FILE__ ) . '/groups.php';
}

// Include People
if ( $coreoptions['people'] == '1' ) { 
require_once plugin_dir_path( __FILE__ ) . '/people.php';
}
// Include Photo Albums
if ( $coreoptions['photos'] == '1' ) { 
require_once plugin_dir_path( __FILE__ ) . '/albums.php';
}

// Include Widget Content
if ( $coreoptions['widgetcontent'] == '1' ) { 
require_once plugin_dir_path( __FILE__ ) . '/widget-content.php';
}

// Include Widgets
require_once plugin_dir_path( __FILE__ ) . '/widgets.php';

// Include Meta Box Class
require_once plugin_dir_path( __FILE__ ) . '/meta-box/init.php';

// Implement Shortcodes
require plugin_dir_path( __FILE__ ) . 'shortcodes/shortcode-functions.php'; // Main shortcode functions
require plugin_dir_path( __FILE__ ) . 'shortcodes/mce/churchpack_shortcodes_tinymce.php'; // Add mce buttons to post editor	

// Add image sizes
function wpfc_churchpack_images() {
	add_image_size( 'churchpack_tiny', 50, 50, true );
	add_image_size( 'churchpack_thumb', 210, 125, true );
}
add_action("admin_init", "wpfc_churchpack_images");

// Add CSS to header
function wpfc_churchpack_styles() {
	wp_register_style('church-pack', plugins_url('/church-pack.css', __FILE__) ); 
	wp_enqueue_style( 'church-pack' );
}
add_action( 'wp_enqueue_scripts', 'wpfc_churchpack_styles' );

// Google maps shortcode
function cp_googlemaps ( $atts ) {

    extract( shortcode_atts( array(
         'width' => '638',
         'height' => '210',
         'color' => 'green',
         'zoom' => '15',
         'align' => 'center',
		 'address' => ''
     ), $atts) );

    ob_start();
    
    $address_url = preg_replace( '![^a-z0-9]+!i', '+', $address );
    
    // Display ?>

    <div id="cp-map"><a href="<?php echo 'http://maps.google.com/maps?q=' . $address_url; ?>" target="_blank"><img class="align<?php echo $align; ?> cp-googlemaps" src="http://maps.google.com/maps/api/staticmap?center=<?php echo $address_url; ?>&zoom=<?php echo $zoom; ?>&size=<?php echo $width; ?>x<?php echo $height; ?>&markers=color:<?php echo $color; ?>|<?php echo $address_url; ?>&sensor=false"></a></div><div class="clear"></div>
    
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;

    }

add_shortcode('cp-map', 'cp_googlemaps');
?>