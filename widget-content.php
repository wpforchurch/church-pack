<?php
// Register Post Type
add_action( 'init', 'register_cpt_wpfc_widget_content' );

function register_cpt_wpfc_widget_content() {

    $labels = array( 
        'name' => _x( 'Widget Content', 'wpfc_widget_content' ),
        'singular_name' => _x( 'Widget Content', 'wpfc_widget_content' ),
        'add_new' => _x( 'Add New', 'wpfc_widget_content' ),
        'add_new_item' => _x( 'Add New Widget Content', 'wpfc_widget_content' ),
        'edit_item' => _x( 'Edit Widget Content', 'wpfc_widget_content' ),
        'new_item' => _x( 'New Widget Content', 'wpfc_widget_content' ),
        'view_item' => _x( 'View Widget Content', 'wpfc_widget_content' ),
        'search_items' => _x( 'Search Widget Content', 'wpfc_widget_content' ),
        'not_found' => _x( 'No widget content found', 'wpfc_widget_content' ),
        'not_found_in_trash' => _x( 'No widget content found in Trash', 'wpfc_widget_content' ),
        'parent_item_colon' => _x( 'Parent Widget Content:', 'wpfc_widget_content' ),
        'menu_name' => _x( 'Widget Content', 'wpfc_widget_content' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Allows specific content to be added to widget areas',
        'supports' => array( 'title', 'editor', 'revisions' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => plugins_url('/img/notebooks.png', __FILE__),
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'wpfc_widget_content', $args );
}
// End Register Post Type

// Widget Content Widget
class wpfc_content_Widget extends WP_Widget {


    function wpfc_content_Widget() {
        $widget_ops = array('classname' => 'widget_content', 'description' => __( 'Display Widget Content you\'ve already created.', 'churchpack') );
		parent::__construct('widget-content', __('Display Widget Content'), $widget_ops);
		$this->alt_option_name = 'widget_content_entries';
    }

    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $number = $instance['number'];
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
						<div class="textwidget">
						<?php
						$my_id = $number;
						$post_id_widget = get_post($my_id);
						$content = $post_id_widget->post_content;
						$content = apply_filters('the_content', $content);
						$content = str_replace(']]>', ']]>', $content);
						echo $content;
						?>
						</div>
						
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
	$instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {				
        $title = esc_attr($instance['title']);
        $number = esc_attr($instance['number']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'churchpack'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            <p>
            	<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Select Widget Content to display:', 'churchpack'); ?></label>
            		<select class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>">
            		<?php $linkposts = get_posts('numberposts=10000&post_type=wpfc_widget_content');
            		foreach($linkposts as $linkentry) :
					$linkvalue = $linkentry->ID;
					echo '<option';
					if ($number == $linkvalue){
					echo ' selected="selected"';
					}
					echo ' value="', $linkvalue , '">', $linkentry->post_title , '</option>';					
					endforeach;	
					?>
				</select> 
            </p>
		<?php 
    }

} // class wpfc_content_Widget
add_action('widgets_init', create_function('', 'return register_widget("wpfc_content_Widget");'));
// End Widget Content
?>